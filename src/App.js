import './App.css';
import {useState} from "react";
import Modal from "./Components/UI/Modal/Modal";
import ModalInfo from "./Components/ModalInfo/ModalInfo";
import Alert from './Components/UI/Alert/Alert';

function App() {

  const [showModal,setShowModal] = useState(false);

  const [showAlert,setShowAlert] = useState(false);

  const showModalOnClick = ()=>{
    setShowModal(true);
  }

  const closeModalOnClick = ()=>{
        setShowModal(false);
  }

  const continueHandler = ()=>{
        alert('Hello!');
  }

  const alertCloseHandler = ()=>{
        setShowAlert(false);
  }

  return (
    <div className="App">

      <button onClick={showModalOnClick} className='click'> Click me to show modal</button>
      <Modal
          show = {showModal}
          title = 'Hello there!'
          cancel = {closeModalOnClick}

      >
          <ModalInfo
            onCancel ={closeModalOnClick}
            onContinue = {continueHandler}
          />
      </Modal>
        <Alert 
            type = 'warning'
            dismiss = {continueHandler}
        >
            Some warning text
        </Alert>

        <Alert
            type = 'primary'
        >
            Some lorem text
        </Alert>

        <Alert
            type = 'success'
        >
            Some lorem text
        </Alert>

        <Alert
            type = 'danger'
        >
            Some lorem text
        </Alert>
    </div>
  );
};

export default App;
