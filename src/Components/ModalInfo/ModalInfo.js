import React from 'react';
import Button from "../UI/Button/Button";


const ModalInfo = props => {
    return (
        <div>
            <p>Some information about something that should be shown in Modal!</p>
            <Button type = 'Danger' onClick = {props.onCancel}>CANCEL</Button>
            <Button type = 'Success' onClick = {props.onContinue}>CONTINUE</Button>
        </div>
    );
};

export default ModalInfo;