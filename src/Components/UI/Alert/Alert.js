import React from 'react';
import './Alert.css';

const Alert = props => {
    
    return (
        <div className={props.type}>

                <button style={{
                    opacity: props.dismiss === 'undefined' ? '0' : '1'
                }
                } >x</button>

                {props.children}
           
        </div>
    );
};

export default Alert;